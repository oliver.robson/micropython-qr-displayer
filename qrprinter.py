from ili934xnew import ILI9341, color565
from machine import Pin, SPI
import m5stack
import framebuf
import qr

# Colours
BLACK = 0x0
WHITE = 0xffffff

# Config
BORDER = 2

class QrPrinter:
    def __init__(self):
        self.power = Pin(m5stack.TFT_LED_PIN, Pin.OUT)
        self.power.value(1)

        self.spi = SPI(
            2,
            baudrate=40000000,
            miso=Pin(m5stack.TFT_MISO_PIN),
            mosi=Pin(m5stack.TFT_MOSI_PIN),
            sck=Pin(m5stack.TFT_CLK_PIN))

        self.display = ILI9341(
            self.spi,
            cs=Pin(m5stack.TFT_CS_PIN),
            dc=Pin(m5stack.TFT_DC_PIN),
            rst=Pin(m5stack.TFT_RST_PIN))
        self.display.erase()

    def print(self, qr_str):
        self.display.fill_rectangle(0, 0, self.display.width, self.display.height, color=WHITE)

        # Create loading screen
        dotsize = 10
        numdots = 3
        for dot in range(3):
            self.display.fill_rectangle(
                int(self.display.width * (dot+1) / (numdots+1) - dotsize / 2),
                int(self.display.height / 2 - dotsize / 2),
                dotsize,
                dotsize,
                BLACK
            )

        # Create QR code
        qr_tuple = qr.encode_text2(qr_str)

        qr_size = len(qr_tuple)
        qr_size += 2 * BORDER # Allow for white border
        pix_per_el = int(self.display.height / qr_size)

        qr_size = pix_per_el * len(qr_tuple)
        offset_x = int((self.display.width-qr_size)/2)
        offset_y = int((self.display.height-qr_size)/2)

        buf = bytearray(qr_size * qr_size)
        fb = framebuf.FrameBuffer(buf, qr_size, qr_size, framebuf.MONO_VLSB)
        fb.fill(WHITE)

        for i, row in enumerate(qr_tuple):
            for j, val in enumerate(row):
                if val:
                    fb.fill_rect(
                        j*pix_per_el,
                        i*pix_per_el,
                        pix_per_el,
                        pix_per_el,
                        BLACK
                    )

        self.display.blit(fb, offset_x, offset_y, qr_size, qr_size)
        del(buf)
        del(fb)
