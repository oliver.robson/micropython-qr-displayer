import qrprinter
from microWebSrv import MicroWebSrv
import network
import sixnibblename

my_name = sixnibblename.get()

# Create network
# network.WLAN(network.AP_IF).active(False)
# sta_if = network.WLAN(network.STA_IF)
# sta_if.active(True)
# sta_if.connect('pyconau', 'pythonmeanspython3')
# while not sta_if.isconnected():
#     pass
# ip = sta_if.ifconfig()[0]
ap_if = network.WLAN(network.AP_IF)
ap_if.config(essid='QR-Printer_%s' % my_name)
ap_if.active(True)
ip = ap_if.ifconfig()[0]
print('Server name: %s, Server IP: %s' % (my_name, ip))
my_url = 'http://%s/' % ip

# Print QR code
printer = qrprinter.QrPrinter()
def print_url():
    printer.print(my_url)
print_url()

def string_clean(str):
    if len(str) > 99:
        return False
    return True

# Views
@MicroWebSrv.route('/', 'GET')
@MicroWebSrv.route('/', 'POST')
def encodeGetHandler(httpClient, httpResponse):
    valid_str = False
    htmlBody = '<html><body style="font-family:sans-serif"><h1>QR Code Generator</h1>'
    if httpClient.GetRequestMethod() == 'POST':
        in_str = str(httpClient.ReadRequestPostedFormData()['input_string'])
        valid_str = string_clean(in_str)

        if valid_str:
            if in_str:
                htmlBody += '<p style="color:green">Thank you for your input!</p>'
            else:
                htmlBody += '<p style="color:green">No string received - printing IP QR!</p>'
        else:
            htmlBody += '<p style="color:red">Invalid string :( Try again!</p>'
    htmlBody += '<form action="" method="post">'
    htmlBody += 'String to encode: <input type="text" name="input_string">'
    htmlBody += '<input type="submit" value="Submit"></form></body></html>'
    httpResponse.WriteResponseOk(contentType='text/html', content=htmlBody)
    if valid_str:
        if in_str:
            printer.print(in_str)
        else:
            print_url()

# Start web server
mws = MicroWebSrv()
mws.SetNotFoundPageUrl(my_url)
mws.Start(threaded=False)
