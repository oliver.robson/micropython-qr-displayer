# QR Displayer

A micro web server to present a web endpoint that will generate a QR code and present it on the devices display.

Designed for running on the ESP32 based m5stack.

## Prerequesites

Like all good modern software, this project was built on the back of people that I let do the hard stuff so I could do the exciting easier part!

You may want to `mpy-cross` some of these before putting them on your device, as they're big (ignore this message if you have an external RAM chip).

### MicroWebSrv

A web framework for MicroPython - https://github.com/jczic/MicroWebSrv

We need `microWebSrv.py` specifically - put it on the target device.

### micropython-ili9341

A MicroPython driver for the ili9341 display driver - https://github.com/jeffmer/micropython-ili9341

We need both `ili934xnew.py` and `m5stack.py` - put them on the target device.

We also locally modified `ili934xnew.py` to remove the import to `glcdfont` and the `self._font = glcdfont` reference to it to save memory - oops, probably should have done that better. `FIXME`. It might work without doing this and bringing in fonts, not sure! Keep in mind that weird behaviour may be you running out of memory!

### Six Nibble Name

A (Micro)Python library for creating names of devices from serial numbers - https://github.com/HowManyOliversAreThere/six-nibble-name

We need `sixnibblename.py` - put it on the target device.

### MicroPython

The most important part of this whole operation - https://micropython.org

We're actually using a version of MicroPython with QR code generation built into it, watch this space for how you may have access to such a thing yourself.

## Use

1. Run your device, and a WiFi network should appear named `QR-Printer_Name`, where `Name` will be the automatically generated name for your device
2. Connect to this access point, and then scan the QR code on the device and go to the address contained within (probably https://192.168.4.1/)
3. Enter the string to encode and hit the button!

### Notes

- Current maximum string length of 99
- Setting an empty string will make the device re-display it's IP address as the QR code
